import customtkinter as ctk
import subprocess
import sys

app = ctk.CTk()
app.geometry("400x400")
app.title("PDF Scanner")

"""
status_label = ctk.CTkLabel(master=app, text="Status: IDLE")
status_label.place(relx=0.5, rely=0.4, anchor=ctk.CENTER)
"""
scan_button = ctk.CTkButton(master=app, text="Scan File")
scan_button.place(relx=0.5, rely=0.5, anchor=ctk.CENTER)

process = None

def main():
    scan_button.configure(command=start_scanner)
    app.mainloop()

def start_scanner():
    command = ["python", "scanner.py"]
    #status_label.configure(text="Status: PROCESSING")
    global process
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    app.after(120000, close_app)
    
def close_app():
    sys.exit()

if __name__ == "__main__":
    main()
