from PIL import Image
from pypdf import PdfReader, PdfWriter
from tkinter import StringVar, TOP
import customtkinter as ctk
from tkinterdnd2 import TkinterDnD, DND_ALL
from pdf2image import convert_from_path
from pytesseract import pytesseract
import re
import sys
import os
import threading
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)

GLOBAL_ZOOM_INIT = .85
GLOBAL_OFFSET_INIT = [0, 0]
LABEL_SPACING = 0.35

ROTATIONS = []
ZOOM_FACTORS = []
OFFSET_FACTORS = []
DOCUMENT_PATH = ""
OUTPUT_PATH = ""

PATH_TO_TESERRACT = r"includes\\Tesseract-OCR\\tesseract.exe"

pattern1 = r".*([A-Z]|9){1,2}(-|\.).*([0-9]).*"
pattern2 = r".*[A-Z]([0-9]|[ls]).*"

label_regex = r"([A-Z]|[0-9]){1,4}(-|\.){0,1}([0-9]){1,2}"

documentImage = None
documentProcessed = False

pageImages = []
pageNames = []
prevNames = []

load_thread = None

exampleImageIndex = 0
numPages = 0
# Scanner app components --------------------------------------------
class StatsFrame(ctk.CTkScrollableFrame):
        def __init__(self, master, **kwargs):
            super().__init__(master, **kwargs)

            self.main_label = ctk.CTkLabel(master=self, text="")
            self.main_label.grid(row=0, column=0, padx=20)
            self.main_label.configure(justify="left")

        def set_names(self, label_text):
            self.main_label.configure(text=label_text)

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("blue")

app = ctk.CTk()
app.geometry("300x200")
button = ctk.CTkButton(master=app, text="Process Document")
confirmButton = ctk.CTkButton(master=app, text="Set Page Names")
imageFrame = ctk.CTkLabel(master=app, text="Processing PDF data...")
namesLabel = StatsFrame(master=app, width=200, height=500)
namesHeader = ctk.CTkLabel(master=app, text="Detected Names:")
examplePageNumLabel = ctk.CTkLabel(master=app, text="Page 1")
statisticsLabel = ctk.CTkLabel(master=app, text="Scan Statistics:")

globalZoomSlider = ctk.CTkSlider(master=app, from_=80, to=100)
globalZoomLabel = ctk.CTkLabel(master=app, text="Global Zoom")

imageSelectLabel = ctk.CTkLabel(master=app, text="Select Image")
imageSelectButton = ctk.CTkSegmentedButton(master=app, values=["<", "|", ">"])

widthSlider = ctk.CTkSlider(master=app, from_=80, to=100)
widthSliderLabel = ctk.CTkLabel(master=app, text="Zoom")
offsetSliderH = ctk.CTkSlider(master=app, from_=0, to=200)
offsetSliderLabelH = ctk.CTkLabel(master=app, text="Horizontal Offset")
offsetSliderV = ctk.CTkSlider(master=app, from_=0, to=200)
offsetSliderLabelV = ctk.CTkLabel(master=app, text="Vertical Offset")

bookmark_extension_input = ctk.CTkEntry(master=app, placeholder_text="extra bookmark extension")
bookmark_extension_label = ctk.CTkLabel(master=app, text="Additional Extension: ")

goto_input = ctk.CTkEntry(master=app, placeholder_text="page#")
goto_button = ctk.CTkButton(master=app, text="Go To")

rotate_image_button = ctk.CTkSegmentedButton(master=app, values=["<", "|", ">"])
rotate_image_label = ctk.CTkLabel(master=app, text="Rotate Image")

# -------------------------------------------------------------------



def populate_app():
    app.geometry("1600x900")

    button.place(relx=0.5, rely=0.9, anchor=ctk.CENTER)

    imageFrame.place(relx=0.5, rely=0.4, anchor=ctk.CENTER)
    imageFrame.configure(text=" ")

    namesLabel.place(relx=0.85, rely=0.5, anchor=ctk.E)
    namesHeader.place(relx=0.78, rely=0.2, anchor=ctk.CENTER)

    examplePageNumLabel.place(relx=0.5, rely=0.1, anchor=ctk.CENTER)
    goto_input.place(relx=0.5, rely=0.15, anchor=ctk.CENTER)
    goto_button.place(relx=0.4, rely=0.15, anchor=ctk.CENTER)

    statisticsLabel.place(relx=0.15, rely=0.5, anchor=ctk.E)

    bookmark_extension_input.place(relx=0.5, rely=0.2, anchor=ctk.CENTER)
    bookmark_extension_label.place(relx=0.4, rely=0.2, anchor=ctk.CENTER)

    rotate_image_button.place(relx=0.5, rely=0.6, anchor=ctk.CENTER)
    rotate_image_label.place(relx=0.4, rely=0.6, anchor=ctk.CENTER)
    color = rotate_image_button.cget("unselected_color")
    rotate_image_button.configure(selected_color=color, selected_hover_color=color)

    globalZoomSlider.place(relx=0.5, rely=0.65, anchor=ctk.CENTER)
    globalZoomLabel.place(relx=LABEL_SPACING, rely=0.65, anchor=ctk.CENTER)

    imageSelectButton.place(relx=0.5, rely=0.7, anchor=ctk.CENTER)
    imageSelectLabel.place(relx=0.4, rely=0.7, anchor=ctk.CENTER)
    color = imageSelectButton.cget("unselected_color")
    imageSelectButton.configure(selected_color=color, selected_hover_color=color)

    widthSlider.set(90)
    widthSlider.place(relx=0.5, rely=0.75, anchor=ctk.CENTER)
    widthSliderLabel.place(relx=LABEL_SPACING, rely=0.75, anchor=ctk.CENTER)
    offsetSliderH.set(0)
    offsetSliderH.place(relx=0.5, rely=0.8, anchor=ctk.CENTER)
    offsetSliderLabelH.place(relx=LABEL_SPACING, rely=0.8, anchor=ctk.CENTER)
    offsetSliderV.set(0)
    offsetSliderV.place(relx=0.5, rely=0.85, anchor=ctk.CENTER)
    offsetSliderLabelV.place(relx=LABEL_SPACING, rely=0.85, anchor=ctk.CENTER)

    display_image()

    ctk.set_appearance_mode("System")
    ctk.set_default_color_theme("blue")

    button.configure(command=init_process_thread)
    goto_button.configure(command=goto_page)
    widthSlider.configure(command=update_bounds)
    offsetSliderH.configure(command=update_offset_horizontal)
    offsetSliderV.configure(command=update_offset_vertical)
    imageSelectButton.configure(command=change_example_image)
    rotate_image_button.configure(command=rotate_image)
    globalZoomSlider.configure(command=global_zoom_update)


# File picker components --------------------------------------------

class Tk(ctk.CTk, TkinterDnD.DnDWrapper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.TkdndVersion = TkinterDnD._require(self)

filePickerApp = None

#--------------------------------------------------------------------

def main():
    global filePickerApp
    filePickerApp = Tk()

    filePickerApp.geometry("300x300")

    nameVar = StringVar()

    fileEntry = ctk.CTkEntry(filePickerApp, height=250, width=250, justify="center")
    fileEntry.place(relx=0.5, rely=0.5, anchor=ctk.CENTER)
    fileEntry.insert(0, "Drag and Drop PDF Here")
    fileEntry.configure(state="disabled")

    #pathLabel = ctk.CTkLabel(master=filePickerApp, text="^ Drag and Drop File Above ^")
    #pathLabel.place(relx=0.5, rely=0.6, anchor=ctk.CENTER)

    filePickerApp.title("Blueprint Scanner")

    fileEntry.drop_target_register(DND_ALL)
    fileEntry.dnd_bind("<<Drop>>", open_document)

    filePickerApp.mainloop()

def start_scanner():
    #open_document_dialog()
    global load_thread
    load_thread = threading.Thread(target=split, name="file_load")
    load_thread.start()

    imageFrame.place(relx=0.5, rely=0.4, anchor=ctk.CENTER)

    app.title("Blueprint Scanner")
    app.state('iconic')
    app.after(1000, check_async_load)
    app.mainloop()

def check_async_load():
    global load_thread
    if load_thread.is_alive():
        app.after(100, check_async_load)
    else:
        populate_app()

def open_new_pdf():
    os.system(get_output_name())

def update_bounds(value):
    value /= 100
    global ZOOM_FACTORS, exampleView
    ZOOM_FACTORS[exampleImageIndex] = value
    display_image()

def global_zoom_update(value):
    global ZOOM_FACTORS
    for i in range(len(ZOOM_FACTORS)):
        ZOOM_FACTORS[i] = value / 100
    display_image()

def update_offset_horizontal(value):
    global OFFSET_FACTORS
    OFFSET_FACTORS[exampleImageIndex][0] = value
    display_image()

def update_offset_vertical(value):
    global OFFSET_FACTORS
    OFFSET_FACTORS[exampleImageIndex][1] = value
    display_image()

def goto_page():
    page_num = goto_input.get()
    page_num = int(page_num)

    global exampleImageIndex
    exampleImageIndex = (page_num - 1) % numPages

    change_example_image(" ")

def change_example_image(value):
    imageSelectButton.set("|")
    global exampleImageIndex

    if (value == "<"):
        exampleImageIndex -= 1
        exampleImageIndex %= numPages

    elif (value == ">"):
        exampleImageIndex += 1
        exampleImageIndex %= numPages

    zoom = ZOOM_FACTORS[exampleImageIndex]
    offsetH = OFFSET_FACTORS[exampleImageIndex][0]
    offsetV = OFFSET_FACTORS[exampleImageIndex][1]

    widthSlider.set(zoom * 100)
    offsetSliderH.set(offsetH)
    offsetSliderV.set(offsetV)

    examplePageNumLabel.configure(text="Page " + str(exampleImageIndex + 1) + "/" + str(numPages))
    display_image()


def display_image():
    #print("index = ",exampleImageIndex)
    img = pageImages[exampleImageIndex]
    cropped = crop_image(img, exampleImageIndex)
    rotated = cropped.rotate(ROTATIONS[exampleImageIndex])

    #print("finished cropping image : ", cropped)

    exampleView = ctk.CTkImage(dark_image=rotated, size=(300, 300))

    imageFrame.configure(image=exampleView, text="")

def disable_elements():
    button.configure(state="disabled")
    confirmButton.configure(state="disabled")
    imageSelectButton.configure(state="disabled")
    rotate_image_button.configure(state="disabled")
    bookmark_extension_input.configure(state="disabled")
    widthSlider.configure(state="disabled")
    offsetSliderH.configure(state="disabled")
    offsetSliderV.configure(state="disabled")

    processing_image = Image.open("includes/processing.png")
    processing_view = ctk.CTkImage(dark_image=processing_image, size=(300, 300))
    imageFrame.configure(image=processing_view, text="")

def enable_elements():
    button.configure(state="normal")
    confirmButton.configure(state="normal")
    imageSelectButton.configure(state="normal")
    widthSlider.configure(state="normal")
    offsetSliderH.configure(state="normal")
    offsetSliderV.configure(state="normal")
    rotate_image_button.configure(state="normal")
    bookmark_extension_input.configure(state="normal")

    display_image()

def init_process_thread():
    global process_thread
    process_thread = threading.Thread(target=process_document, name="file_processing")
    process_thread.start()
    disable_elements()
    imageFrame.configure()

def process_document():

    num = 1
    global prevNames, pageNames
    prevNames = pageNames.copy()
    pageNames.clear()

    for img in pageImages:
        #print("reading img ", num)
        if len(prevNames) > 0 and prevNames[num - 1] != "No Match":
            pageNames.append(prevNames[num - 1])
            num += 1
            continue
        cropped = crop_image(img, num - 1)
        text = read_image(cropped)
        res = None#re.search(label_regex, text)

        if (res == None):
            res = re.search(pattern1, text)
        if (res == None):
            res = re.search(pattern2, text)
        if (res != None and len(res.group()) < 6):
            label = res.group()
            label = label.lower()
            label = label.replace('.', '_')
            pageNames.append(label)
        else:
            if len(prevNames) > 0 and prevNames[num - 1] != "No Match":
                pageNames.append(prevNames[num -1])
            else:
                pageNames.append("No Match")
        num += 1
    display_names()
    show_confirm_button()
    enable_elements()

def show_confirm_button():
    confirmButton.configure(command=set_bookmarks)
    confirmButton.place(relx=0.5, rely=0.10, anchor=ctk.CENTER)

def display_stats(m, t):
    match_rate = m / t * 100
    label = "Statistics:\n"
    label += "Matched Names: " + str(m) + "\n"
    label += "Total Pages: " + str(t) + "\n"
    label += f"Match Rate: {match_rate:.2f}%\n"
    statisticsLabel.configure(text=label)

def display_names():
    names = "Detected Page Names:\n"
    i = 1
    matched = 0
    for name in pageNames:
        names += "Page " + str(i) + ")\t\t" + name + "\n"
        if (name != "No Match"):
            matched += 1
        i+=1
    namesLabel.set_names(names)
    display_stats(matched, i-1)

def crop_image(im, index):
    im_index = index
    width, height = im.size
    left = width * ZOOM_FACTORS[im_index] - OFFSET_FACTORS[im_index][0]
    top = height * ZOOM_FACTORS[im_index] - OFFSET_FACTORS[im_index][1]
    right = width - OFFSET_FACTORS[im_index][0]
    bottom =  height - OFFSET_FACTORS[im_index][1]
    return im.crop((left, top, right, bottom))

def rotate_image(value):
    global ROTATIONS
    rotate_image_button.set("|")

    if value == "<":
        ROTATIONS[exampleImageIndex] -= 90
    elif value == ">":
        ROTATIONS[exampleImageIndex] += 90

    display_image()

def read_image(img):
    pytesseract.tesseract_cmd = PATH_TO_TESERRACT

    text = pytesseract.image_to_string(img)
    return text[:-1]

def open_document(event):
    global DOCUMENT_PATH
    DOCUMENT_PATH = str(event.data)
    DOCUMENT_PATH = DOCUMENT_PATH.replace('{', '')
    DOCUMENT_PATH = DOCUMENT_PATH.replace('}', '')

    filePickerApp.destroy()
    start_scanner()

def open_document_dialog(event):
    global DOCUMENT_PATH
    DOCUMENT_PATH = ctk.filedialog.askopenfilename()
    print(DOCUMENT_PATH)

def get_output_name():
    return OUTPUT_PATH

def split():
    global DOCUMENT_PATH

    images = convert_from_path(DOCUMENT_PATH, poppler_path="includes/poppler/Library/bin", dpi=100)
    """
    if not os.path.exists("images"):
        print("making images directory...")
        os.makedirs("images")
    """
    global numPages
    numPages = 0
    for i in images:
        ZOOM_FACTORS.append(GLOBAL_ZOOM_INIT)
        OFFSET_FACTORS.append(GLOBAL_OFFSET_INIT)
        ROTATIONS.append(0)
        pageImages.append(i)
        numPages += 1

    examplePageNumLabel.configure(text="Page " + str(exampleImageIndex + 1) + "/" + str(numPages))

def set_bookmarks():
    pdf_path = ( DOCUMENT_PATH )
    input_pdf = PdfReader(pdf_path)
    output_pdf = PdfWriter()

    for i in range(len(pageNames)):
        page_num = "{:03d}".format(i + 1)

        bookmarkTitle = page_num + " " + pageNames[i] + "-" + bookmark_extension_input.get()
        page = input_pdf.pages[i]
        output_pdf.add_page(page)
        output_pdf.add_outline_item(bookmarkTitle, i)

    file_types = [('All files', '*.*'),('Pdf' , '*.pdf')]
    global OUTPUT_PATH
    index = DOCUMENT_PATH.rfind('/')
    filename = DOCUMENT_PATH[index + 1:len(DOCUMENT_PATH)-4]

    OUTPUT_PATH = ctk.filedialog.asksaveasfilename(initialfile = filename + "-formatted", title="Save the pdf", filetypes=file_types, defaultextension=".pdf")
    output_pdf.write(get_output_name())

    open_pdf_thread = threading.Thread(target=open_new_pdf, name="open_pdf")
    open_pdf_thread.start()

    app.destroy()
    main()

if __name__ == "__main__":
    main()
    #start_scanner()
